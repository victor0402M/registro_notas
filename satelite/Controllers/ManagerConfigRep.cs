﻿using System;
using System.Configuration;
using Newtonsoft.Json;


namespace satelite.Controllers
{
    public class ManagerConfigRep 
    {
        /// <summary>
        /// Gets the page configuration for Scs.
        /// </summary>
        /// <param name="ViewBag">The view bag.</param>
        public static void GetPageConfig(dynamic ViewBag)
        {
            ViewBag.inicio = ConfigurationManager.AppSettings["ida:StartScs"];
            ViewBag.reviSoliReal = ConfigurationManager.AppSettings["ida:CheckServiceRequest"];
            ViewBag.subTitulo = ConfigurationManager.AppSettings["ida:SubTitleNameRep"];
        }

        /// <summary>
        /// Get de identification test
        /// </summary>
        /// <returns></returns>
        public static string GetTestIdentification()
        {
            return ConfigurationManager.AppSettings["ida:IdentificationTest"];
        }


    }
}