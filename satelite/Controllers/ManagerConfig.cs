﻿using System;
using System.Configuration;
using Newtonsoft.Json;


namespace satelite.Controllers
{
    public class ManagerConfig
    {
        public const string DATA_PROVIDER = "Oracle.ManagedDataAccess.Client";

        
        /// <summary>
        /// Retrieve the text to be printed in the certificate validation view
        /// </summary>
        /// <returns></returns>
        public static string GetDataBaseConnectionString()
        {
            return ConfigurationManager.AppSettings["ida:TextValidation"];
        }


        public static decimal GetValueAprove()
        {
            return  Convert.ToDecimal(ConfigurationManager.AppSettings["ida:Aprov"]);
        }

    }
}