﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;

namespace satelite.Controllers
{
    public class ErrorController : Controller
    {
        /// <summary>
        /// Rendering to main view for errors
        /// </summary>
        /// <param name="token"> Error code</param>
        /// <returns></returns>
        [HandleError]
        public ActionResult Index(string token)
        {
            ManagerConfigRep.GetPageConfig(ViewBag);
            return View();
        }


        /// <summary>
        /// Render to view when error is page not found
        /// </summary>
        /// <returns></returns>
        [HandleError]
        public ActionResult NotFound()
        {
            ManagerConfigRep.GetPageConfig(ViewBag);
            return View();
        }


        /// <summary>
        /// Render to view when error is page not allowed
        /// </summary>
        /// <returns></returns>
        [HandleError]
        public ActionResult Forbidden()
        {
            ManagerConfigRep.GetPageConfig(ViewBag);
            return View("~/Views/Error/ErrorNoAccess.cshtml");
        }


        /// <summary>
        /// Paracial rendering of extra content in the view.
        /// </summary>
        /// <returns>PartialView</returns>
        [HttpGet]
        public PartialViewResult ExtraContent()
        {
            return PartialView("_ExtraContent");
        }

    }
}