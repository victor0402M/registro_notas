﻿using System.Collections.Generic;
using System.Web.Mvc;
using Newtonsoft.Json;
using log4net;
using satelite.Models;
using System;
using System.Net;


namespace satelite.Controllers
{
    public class CalificationsController : Controller
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(CalificationsController));

        /// <summary>
        /// Displays the start page of the application for the generation of certificates
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }



        /// <summary>
        /// Renderizacion paracial de contentido extra en la vista.
        /// </summary>
        /// <returns>PartialView</returns>
        public PartialViewResult ExtraContent()
        {
            return PartialView("_ExtraContent");
        }


        [HttpPost]
        public ActionResult GetStudeins()
        {
            try
            {
                var studenList = new EstudentsDAO().GetStudenstList();

                List<string[]> data = new List<string[]>();

                foreach (var registro in studenList)
                {
                    data.Add(GetRegist(registro));
                }

                string json = JsonConvert.SerializeObject(new { data });
                return Json(json, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Info(string.Format(ErrorMessage.ERROR_DATATABLE, ex.Message));
                throw new Exception(string.Format(ErrorMessage.ERROR_DATATABLE, ex.Message));
            }
            
        }


        private string[] GetRegist(dynamic registro)
        {
            string idStudent = Convert.ToString(registro.ID_ESTUDIANTE),
                    nameStudent = (registro.NOMBRE_ESTUDIANTE == null) ? "SN" : registro.NOMBRE_ESTUDIANTE,
                    actions = "<a class=\"link-modal\" title = \"Cargar adjunto\" href = \"javascript:void(0)\" onclick = \"OpenModalCalification(\'" + idStudent + "\')\" > " +
                        "<i class=\"fas fa-file-upload medium-icons color-gey\"></i>" +
                        "</a>",
                     promedio = "NA",
                     estadoEst = "NA";
            float homeworkCalification = (registro.CALIFICACION_DEBERES == null) ? 0 : registro.CALIFICACION_DEBERES,
                testCalification = (registro.CALIFICACION_EXAMEN == null) ? 0 : registro.CALIFICACION_EXAMEN;

            if (homeworkCalification > 0 && homeworkCalification > 0)
            {
                decimal calcProm = Convert.ToDecimal((homeworkCalification + homeworkCalification) / 2);
                promedio = Convert.ToString(calcProm);
                estadoEst = (calcProm >= ManagerConfig.GetValueAprove()) ? "APROBADO" : "REPROBADO"; 
            }

            var dataRegis = new string[]
                {
                    nameStudent,
                    Convert.ToString(homeworkCalification),
                    Convert.ToString(testCalification),
                    promedio,
                    estadoEst,
                    actions
                };

            return dataRegis;
        }


        [HttpPost]
        public ActionResult Save()
        {
            string data = String.Empty;
            string json = String.Empty;
            try
            {
                string dataTestRequestService = "{\"studentId\":\"" + HttpContext.Request.Params.Get("code") +
                   "\" ,\"caliHome\":\"" + HttpContext.Request.Params.Get("caliHome") +
                   "\" ,\"caliExam\":\"" + HttpContext.Request.Params.Get("caliExam") + "\" }";
                
                var dataRequestService = JsonConvert.DeserializeObject<dynamic>(dataTestRequestService);

                if (ValidateData(dataRequestService))
                {
                    var studenList = new EstudentsDAO().SetCalification(dataRequestService);

                    data = "{\"status\":\"" + studenList + "\" }";

                    json = JsonConvert.SerializeObject(new { data });
                }
                else
                {
                    data = "{\"status\":\"" + false + "\" ,\"er\":\"" + ErrorMessage.ERROR_DATA +
                        "\" }";
                    json = JsonConvert.SerializeObject(new { data });
                }

                return Json(json, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Info(string.Format(ErrorMessage.ERROR_REGISTRO, ex.Message));
                data = "{\"status\":\"" + false + "\" ,\"er\":\"" + ErrorMessage.ERROR_REGISTRO +
                        "\" }";
                json = JsonConvert.SerializeObject(new { data });
                return Json(json, JsonRequestBehavior.AllowGet);
            }
        }


        private bool ValidateData(dynamic dataRequest)
        {
            try {
                int id_est = Convert.ToInt32(dataRequest.studentId.Value);
                int cali_test = Convert.ToInt32(dataRequest.caliExam.Value);
                int cali_home = Convert.ToInt32(dataRequest.caliHome.Value);

                if (cali_test < 0 || cali_test > 10)
                {
                    return false;
                }

                if (cali_home < 0 || cali_home > 10)
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            
        }
















    }
}