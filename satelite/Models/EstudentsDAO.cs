﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using Dapper.Oracle;
using System.Data;
using Oracle.ManagedDataAccess.Types;
using Oracle.ManagedDataAccess.Client;

namespace satelite.Models
{
    public class EstudentsDAO
    {
        private string ListEstudents = "select "+
            "estudiante.ID_ESTUDIANTE, "+
            "NOMBRE_ESTUDIANTE,  "+
            "ID_CALIFICACION,  " +
            "CALIFICACION_DEBERES,  " +
            "CALIFICACION_EXAMEN  from estudiante left join calificacion on calificacion.id_estudiante = estudiante.id_estudiante";

        private string SaveData = "STD_TEST.SAVE_STD";

        public IEnumerable<dynamic> GetStudenstList()
        {
            IEnumerable<dynamic> result = null; 

            using (var conection = ConnectionDB.GetConnection())
            {
                //1. Sql para extraer valores
                string query = ListEstudents;
                //2. Call query
                result = SqlMapper.Query<dynamic>(
                    cnn: conection,
                    sql: query,
                    commandType: CommandType.Text
                ).ToList();
            }         

            return result;
        }



        public bool SetCalification(dynamic parameters)
        {
            bool result = false;

            using (var conection = ConnectionDB.GetConnection())
            {
                //1. Sql para extraer valores
                string query = SaveData;
                var dyParam = new OracleDynamicParameters();
                dyParam.Add(name: "ID_STUD",
                    dbType: OracleMappingType.Int32,
                    direction: ParameterDirection.Input,
                    value: parameters.studentId.Value);
                dyParam.Add(name: "CALI_EXAM",
                    dbType: OracleMappingType.Decimal,
                    direction: ParameterDirection.Input,
                    value: parameters.caliExam.Value);
                dyParam.Add(name: "CALI_HOME",
                    dbType: OracleMappingType.Decimal,
                    direction: ParameterDirection.Input,
                    value: parameters.caliHome.Value);

                dyParam.Add(name: "SN_VALOR",
                    dbType: OracleMappingType.Int32,
                    direction: ParameterDirection.Output);

                conection.Execute(query, dyParam, commandType: CommandType.StoredProcedure);

                result = (dyParam.Get<int>("@SN_VALOR") == 1);
            }

            return result;
        }





    }
}