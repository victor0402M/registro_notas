﻿namespace satelite.Models
{
    public class ErrorMessage
    {
        public static string ERROR_DATATABLE = "ERROR EN CARGA DEL DATATABLE, {0}.";
        public static string ERROR_REGISTRO = "Lo lamentamos, por el momento no se encuentra disponible el registro de notas, favor contactar con el administrador.";
        public static string ERROR_DATA = "Lo lamentamos, los datos enviados son incorrectos, favor corregir y volver a intentar.";

        /// <summary>
        /// Obtiene el mensaje de error a guardar en el log.
        /// </summary>
        /// <param name="error">El error generado.</param>
        /// <param name="data">Los datos.</param>
        /// <param name="origin">El origen.</param>
        /// <returns></returns>
        public static string GetGenerirErrorLogError(string error, string data, string origin)
        {
            string messaje = "ERROR: " + error + " | DATOS: " + data + " | ORIGEN: " + origin;
            return messaje;
        }


    }
}