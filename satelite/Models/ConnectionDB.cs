﻿using System;
using System.Data.Common;
using System.Configuration;

namespace satelite.Models
{
    public class ConnectionDB
    {
        private static DbConnection conn = null;
        private static string ConnectionString = string.Empty;
        private static string DataProvider = string.Empty;


        public static string GetStringConnection()
        {
            if (string.IsNullOrWhiteSpace(ConnectionString))
            {
                //1. Nombre cadena de conexion configurada
                var nameConnectionString = ConfigurationManager.AppSettings[Constants.STRING_DB_CONNECTION];
                if (string.IsNullOrWhiteSpace(nameConnectionString))
                {
                    throw new Exception(string.Format("No existe configuracion de {0}, en AppSettings de cadena de ", Constants.STRING_DB_CONNECTION));
                }
                
                ConnectionString = nameConnectionString;
            }

            return ConnectionString;
        }

        public static string GetDataProvider()
        {
            if (string.IsNullOrWhiteSpace(DataProvider))
            {
                //1. Nombre cadena del de provedor de datos
                var nameDataProviderString = Constants.DATA_PROVIDER;
                if (string.IsNullOrWhiteSpace(nameDataProviderString))
                {
                    throw new Exception(string.Format("No existe configuracion de {0}, en AppSettings de cadena de ", Constants.DATA_PROVIDER));
                }

                DataProvider = nameDataProviderString;
            }
            
            return DataProvider;
        }


        /// <summary>
        /// creates the connection to the database
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <param name="factory">Connection model used for database connection</param>
        /// <returns></returns>
        static public DbConnection GetConnection()
        {
            DbProviderFactory factory;

            try
            {
                factory = DbProviderFactories.GetFactory(GetDataProvider());
                conn = factory.CreateConnection();
                conn.ConnectionString = GetStringConnection();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error en generacion de Proveddor de datos {0}", ex.Message));
            }

            return conn;
        }


    }
}