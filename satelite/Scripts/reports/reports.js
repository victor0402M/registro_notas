﻿var uploadField = document.getElementById("file");
var scholarshipFundValue = '';

window.onload = ShowText();


//------------------REPORTS

//Ocultar texto de "Cargando"
function ShowText() {
    GetUrlRequest();
    $('#load-request').fadeOut();
    $("#pagebody").fadeIn();
}

function GoHome() {
    window.location.href = $("#goHomeLink").attr('href');
}

function ShowLoader(idLoader, btn) {
    $(idLoader).removeClass("d-none");
    $(idLoader).addClass("spinner-border text-secondary");
    $(btn).attr("disabled", true);
}

function HideLoader(idLoader, btn) {
    $(idLoader).attr('class', "d-none");
    $(btn).attr("disabled", false);
}

//Recupera la URL base del sitio web
function GetUrlRequest() {
    version = (url_base.length >= 5) ? "/" + url_base[3] : "";

    if (version == '/Reports') {
        version = '';
    }
    return version;
}


//Limita el texto a mostrarse en los select de la vista
var maxLength = 100;
$('select > option').text(function (i, text) {
    if (text.length > maxLength) {
        return text.substr(0, maxLength) + '...';
    }
});


function ManageMajor() {
    var certificade = $("#certificate :selected").val();
    var indexElement = codeNameCertification.indexOf(certificade)

    if (variationCertification[indexElement] == "False") {
        newOptions = '<option disabled selected value="0">NO REQUERIDO</option>';
        $("#major").html(newOptions)
    } else {
        GetMajor();
    }
}

function ManageAcademicPeriod() {
    var certificade = $("#certificate :selected").val();
    var indexElement = codeNameCertification.indexOf(certificade)

    if (periodCertification[indexElement] == "False") {
        newOptions = '<option disabled selected value="0">NO REQUERIDO</option>';
        $("#academicPeriod").html(newOptions)
    } else {
        GetAcademicPeriod();
    }
}




function GetMajor() {
    var requestData = new FormData();
    requestData.append('certificateCode', $("#certificate :selected").val() );
    $.ajax({
        url: version + '/Reports/GetMajor',
        type: "POST",
        contentType: false,
        processData: false,
        data: requestData,
        success: function (result) {
            //console.log(result);
            data = JSON.parse(result);
            newOptions = '<option disabled selected>Ninguno</option>';
            if (data.status != 'False' && data.status != 'false') {
                data.dataRequest.forEach(
                    element => newOptions = newOptions +
                        "<option value='" + element.guid + "'> " +
                        element.programaAcademico + " </option> "
                );
            }
            $("#major").html(newOptions)
        },
        error: function (error, status) {
            newOptions = '<option disabled selected>Ninguno</option>';
            $("#major").html(newOptions)
        },
        timeout: 50000 // sets timeout to 50 seconds
    });
    
}


function GetAcademicPeriod() {
    var requestData = new FormData();
    requestData.append('certificationCode', $("#certificate :selected").val());
    requestData.append('guidMajor', $("#major :selected").val());
    
    $.ajax({
        url: version + '/Reports/GetAcademicPeriod',
        type: "POST",
        contentType: false,
        processData: false,
        data: requestData,
        success: function (result) {
            //console.log(result);
            data = JSON.parse(result);
            newOptions = '<option disabled selected>Ninguno</option>';
            if (data.status != 'False' && data.status != 'false') {
                data.dataRequest.forEach(
                    element => newOptions = newOptions +
                        "<option value='" + element.guid + "'> " +
                        element.nombre + " </option> "
                );
            }
            $("#academicPeriod").html(newOptions)
        },
        error: function (error, status) {
            newOptions = '<option disabled selected>Ninguno</option>';
            $("#academicPeriod").html(newOptions)
        },
        timeout: 50000 // sets timeout to 50 seconds
    });

}



$('#form-request').submit(function () {
    ValidateData();
    return false;
});


function ValidateData() {
    if ($("#certificate :selected").val() == "Ninguno" ||
        $("#major :selected").val() == "Ninguno" ||
        $("#academicPeriod :selected").val() == "Ninguno")
    {
        $("#warningText").text("Debe selecionar todos los campos para continuar.");
        $("#warning").fadeIn();
        HideLoader("#loaderUploadFile", "#btn-send-utpl");
    } else {

        if ($("#warning").is(":visible") ) {
            $("#warning").fadeOut();
        }
        GenerateCertificate();
    }
}


function GenerateCertificate() {
    ShowLoader("#loaderUploadFile", "#btn-send-utpl");
    var requestData = new FormData();
    requestData.append('certificationCode', $("#certificate :selected").val());
    requestData.append('guidMajor', $("#major :selected").val());
    requestData.append('guidAcademicPeriod', $("#academicPeriod :selected").val());

    $.ajax({
        url: version + '/Reports/GenerateCertificate',
        type: "POST",
        contentType: false,
        processData: false,
        data: requestData,
        dataType: "native",
        xhrFields: {
            responseType: 'blob'
        },
        success: function (result) {
            console.log(result);
            var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
            var diasSemana = new Array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(result);
            var actual_date = new Date();
            link.download = "Certificado_" + diasSemana[actual_date.getDay()] + "_" + meses[actual_date.getMonth()] + "_" + actual_date.getFullYear() + ".pdf";
            link.click();
        },
        error: function (xhr, ajaxOptions, responseText ) {
            console.log(xhr)
            var mensError = 'Lo lamentamos, por el momento no se encuentra disponible la generación del certificado, favor intentar más tarde.';

            if (xhr.status == 403 || xhr.status == '403') {
                mensError = "Lo lamentamos, usted no cuenta con matricula activa para generar el certificado.";
            }
            if (xhr.status == 500 || xhr.status == '500' || xhr.status == 502 || xhr.status == '502') {
                mensError = "Lo lamentamos, ha ocurrido un error al generar el certificado, favor intentar más tarde.";
            }

            if (xhr.status == 502 || xhr.status == '502') {
                const reader = new FileReader();
                const text = "";
                reader.addEventListener('loadend', (e) => {
                    const text = e.srcElement.result;
                    $("#warningText").text(text);
                    $("#warning").fadeIn();
                });
                reader.readAsText(xhr.responseNative);
            } else {
                $("#warningText").text(mensError);
                $("#warning").fadeIn();
            }
            
        },
        complete: function () {
            HideLoader("#loaderUploadFile", "#btn-send-utpl");
        },
        timeout: 100000 // sets timeout to 50 seconds
    });


}



