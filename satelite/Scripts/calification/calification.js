﻿$(document).ready(function () {
    UpdateTable();
});

function UpdateTable() {
    $('#table-califications').DataTable({
        "ajax": {
            "url": "Califications/GetStudeins",
            "type": "POST",
            "dataType": "JSON",
            "dataSrc": function (response) {
                rowsData = $.parseJSON(response);
                return rowsData.data;
            }
        },
    });
}


//$(document).ready(function () {
//    id_plan_port = document.URL;
//    id_plan_port = id_plan_port.split("/");
//    id_plan_port = id_plan_port[id_plan_port.length - 1];

//    Gest_Dato_Tabl_Estr_Orga();

   

//    var elemModalRepo = document.querySelector('#modalReporte');
//    var instance = M.Modal.init(elemModalRepo, {
//        onOpenEnd: function (modal, trigger) {
//            $('#modalReporte form').find("[autofocus]:first").focus();
//            M.updateTextFields();
//        },
//        onCloseEnd: function () {
//            Habi_Boto_Envi_Form('actionformReporte');
//            $('#formReporte')[0].reset();
//            document.getElementById("actionformReporte").setAttribute(
//                'onclick',
//                'Gene_Repo();Desh_Boto_Envi_Form("actionformReporte");');
//        }
//    });


//    var elemModalProd = document.querySelector('#modalProducto');
//    var instanceProd = M.Modal.init(elemModalProd, {
//        onOpenStart: function (modal, trigger) {
//            $('#modalProducto form').find("[autofocus]:first").focus();
//            M.updateTextFields();

//        },
//        onCloseEnd: function () {
//            Habi_Boto_Envi_Form('actionformProducto');
//            $('#formProducto')[0].reset();
//            $('#tituModaProd').html('<h5><b>Agregar un nuevo productos/servicios</b></h5>');
//            document.getElementById("actionformProducto").setAttribute(
//                'onclick',
//                'Crea_Prod();Desh_Boto_Envi_Form("actionformProducto");');
//        }
//    });


 
   
//});


/** Gestión de las configuraciones e información a presentar en las tablas */
function Gest_Dato_Tabl_Estr_Orga() {
    tabla_estructura_organizacional = $('#tabla_estructura_organizacional').DataTable({
        "ajax": {
            "url": document.baseURI + "SUBS_PLAN_Ctrl_Port_Prod/Obte_Dato_Tabl_Estr_Ocup/" + id_plan_port,
            "type": "POST"
        },
        "columnDefs": [
            { title: "CÓDIGO", targets: 0, className: "justify-align" },
            { title: "ENTIDAD", targets: 1 },
            { title: "ATRIBUTO", targets: 2 },
            { title: "ESTATUTO <br> VIGENTE", targets: 3, "width": "5%" },
            { title: "TIPO DE <br> PRODUCTO", targets: 4, "width": "5%" },
            { title: "ESTADO", targets: 5, "width": "5%" },
            { title: "ACCIONES", targets: 6 },
        ],
        "buttons": [],
        "initComplete": function (settings, json) {
            $("div.table-header div.dt-buttons").append("<a onclick ='Gene_Repo_Tabl_Comp(1);' href='javascript:void(0)' id='descargarExcel' class='tooltipped btn btn-floating pulse white descargarExcel' data-position='bottom' data-delay='50' data-tooltip='Clic para exportar tabla en formato .XSL'><i class='tooltipped material-icons green-text' data-position='bottom' data-delay='50' data-tooltip='Clic para exportar la tabla en formato .XSL'>file_download</i></a>&nbsp<a href='javascript:void(0)' id='descargarPDF' onclick ='Gene_Repo_Tabl_Comp(0);' class='tooltipped btn btn-floating pulse white descagarPDF' data-position='bottom' data-delay='50' data-tooltip='Clic para exportar la tabla en formao .PDF'><i class='tooltipped material-icons red-text' data-position='bottom' data-delay='50' data-tooltip='Clic para exportar la tabla en formato .PDF'>file_download</i></a>&nbsp<a onclick ='Most_Busq();' id='most_busq' class='tooltipped btn btn-floating pulse white most_busq' data-position='bottom' data-delay='50' data-tooltip='Clic para mostrar / ocultar filtro de búsqueda avanzada'><i class='material-icons austral-text text-primary-b-100'>touch_app</i></a>&nbsp");

        }
    });
}




function OpenModalCalification(student) {
    $("#code").val(student);
    $("#caliHome").val('');
    $("#caliExam").val('');
    $("#error_text").text('');
    loaderClass = document.getElementById('loaderUploadFile').getAttribute("class");

    if (loaderClass != "d-none") {
        HideLoader("#loaderUploadFile", "#btn-send-utpl");
    }

    $('#uploadFile').modal('show');
}


function ShowLoader(idLoader, btn) {
    $(idLoader).removeClass("d-none");
    $(idLoader).addClass("spinner-border text-secondary");
    $(btn).attr("disabled", true);
}

function HideLoader(idLoader, btn) {
    $(idLoader).attr('class', "d-none");
    $(btn).attr("disabled", false);
}

function UploadFileUser() {
    ShowLoader("#loaderUploadFile", "#btn-send-utpl");
    var requestData = new FormData();
    requestData.append('code', $("#code").val());
    requestData.append('caliHome', $("#caliHome").val());
    requestData.append('caliExam', $("#caliExam").val());
    
    $.ajax({
        url: 'Califications/Save',
        type: "POST",
        contentType: false,
        processData: false, 
        data: requestData,
        success: function (result) {
            console.log(result);
            resultJson = JSON.parse(result);
            data = JSON.parse(resultJson.data);

            if (data.status != 'False' && data.status != 'false')
            {                
                $('#table-califications').dataTable().fnDestroy();
                UpdateTable();
                $('#uploadFile').modal('hide');
            } else {
                document.getElementById("error_text").innerHTML = data.er;
            }
        },
        error: function (error, status) {
            //console.log(xhr);
            console.log(error, status);
            HideLoader("#loaderUploadFile", "#btn-send-utpl");
            document.getElementById("error_text").innerHTML = error;
        },
        complete: function () {
            HideLoader("#loaderUploadFile", "#btn-send-utpl");
        },
    });
   
}